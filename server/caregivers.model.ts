interface CaregiverModel {
    id: string;
    lastName: string;
    firstName: string;
    login: string;
    service: string;
    action: string;
}