interface PatientDataModel {
    date: string;
    duration: string;
    numberOfMiction: string;
    remainingVolumeOfLiquidRinsing: string;
    timeUrined: string;
    totalUrineVolume: string;
    urineVolumeWithEachMiction: string;
}